package Zaaksysteem::Test::Search::Acl;

use Moose;
extends "Zaaksysteem::Test::Moose";

=head1 NAME

Zaaksysteem::Test::Search::ACL - Tests for the the ObjectData ACL role

=head1 DESCRIPTION

These tests confirm that L<Zaaksysteem::Search::Acl> does what it should do.

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Search::Acl;

=cut

use Moose::Util qw(ensure_all_roles);
use Zaaksysteem::Test;
use Zaaksysteem::Search::Acl;

sub test__expand_capabilities {
    my $caps_manage = Zaaksysteem::Test::Search::Acl::Mock::_expand_capabilities({}, 'manage');
    my $caps_write  = Zaaksysteem::Test::Search::Acl::Mock::_expand_capabilities({}, 'write');
    my $caps_read   = Zaaksysteem::Test::Search::Acl::Mock::_expand_capabilities({}, 'read');
    my $caps_search = Zaaksysteem::Test::Search::Acl::Mock::_expand_capabilities({}, 'search');

    is_deeply(
        $caps_manage,
        [qw(manage)],
        "'manage' permission implies 'manage' permission only"
    );
    is_deeply(
        $caps_write,
        [qw(write manage)],
        "'write' permission implies 'manage', 'write' permissions"
    );
    is_deeply(
        $caps_read,
        [qw(read write manage)],
        "'read' permission implies 'manage', 'write', 'read' permissions"
    );
    is_deeply(
        $caps_search,
        [qw(search read write manage)],
        "'search' permission implies 'manage', 'write', 'read', 'search' permissions"
    );

    throws_ok(
        sub { Zaaksysteem::Test::Search::Acl::Mock::_expand_capabilities({}, 'frobnicate'); },
        qr(search/acl/unknown_capability),
        'Trying to expand an unknown capability leads to an exception',
    );
}

sub test_acl_items {
    my @search_args;

    my $mock = Zaaksysteem::Test::Search::Acl::Mock->new(
        result_source => mock_one(
            search_rs => sub {
                push @search_args, [@_];

                my $args = shift;

                return mock_one(
                    as_query => sub { 
                        return {
                            scope      => $args->{scope},
                            groupname  => $args->{groupname},
                            capability => $args->{capability},
                        }
                    }
                );
            },
        )
    );

    throws_ok(
        sub { $mock->acl_items('not_a_capability') },
        qr(search/acl_items/invalid_capability),
        'acl_items throws an exception if an invalid capability is supplied',
    );

    my $override__get_groupnames = override(
        'Zaaksysteem::Test::Search::Acl::Mock::_get_groupnames',
        sub { return ['group1', 'group2']; }
    );
    my $override__get_acl_query = override(
        'Zaaksysteem::Test::Search::Acl::Mock::_get_acl_query',
        sub {
            return {
                instance => [ { entity_type => 'fake1', entity_id => 'one' } ],
                type     => [ { entity_type => 'fake2', entity_id => 'two' } ],
            }
        }
    );

    my @items = $mock->acl_items('read');
    cmp_deeply(
        \@items,
        [
            {
                'acl_groupname' => 'group1',
                'uuid'          => {
                    '-in' => {
                        'capability' => [qw(read write manage)],
                        'groupname'  => 'group1',
                        'scope'      => 'instance'
                    }
                }
            },
            {
                'acl_groupname' => 'group1',
                'class_uuid'    => {
                    '-in' => {
                        'capability' => [qw(read write manage)],
                        'groupname'  => 'group1',
                        'scope'      => 'type'
                    }
                }
            },
            {
                'acl_groupname' => 'group2',
                'uuid'          => {
                    '-in' => {
                        'capability' => [qw(read write manage)],
                        'groupname'  => 'group2',
                        'scope'      => 'instance'
                    }
                }
            },
            {
                'acl_groupname' => 'group2',
                'class_uuid'    => {
                    '-in' => {
                        'capability' => [qw(read write manage)],
                        'groupname'  => 'group2',
                        'scope'      => 'type'
                    }
                }
            },
            {
                'uuid' => {
                    '-in' => {
                        'capability' => [qw(read write manage)],
                        'groupname'  => undef,
                        'scope'      => 'instance'
                    }
                }
            },
            {
                'class_uuid' => {
                    '-in' => {
                        'capability' => [qw(read write manage)],
                        'groupname'  => undef,
                        'scope'      => 'type'
                    }
                }
            },
        ],
        'Generated ACL subquery list is correct'
    );

    cmp_deeply(
        \@search_args,
        [
            [
                {
                    '-or' => [
                        {
                            'entity_id'   => 'one',
                            'entity_type' => 'fake1'
                        }
                    ],
                    'capability' => [qw(read write manage)],
                    'groupname'  => 'group1',
                    'scope'      => 'instance'
                },
                { 'alias' => 'perm_group1_instance' }
            ],
            [
                {
                    '-or' => [
                        {
                            'entity_id'   => 'two',
                            'entity_type' => 'fake2'
                        }
                    ],
                    'capability' => [qw(read write manage)],
                    'groupname'  => 'group1',
                    'scope'      => 'type'
                },
                { 'alias' => 'perm_group1_type' }
            ],
            [
                {
                    '-or' => [
                        {
                            'entity_id'   => 'one',
                            'entity_type' => 'fake1'
                        }
                    ],
                    'capability' => [qw(read write manage)],
                    'groupname'  => 'group2',
                    'scope'      => 'instance'
                },
                { 'alias' => 'perm_group2_instance' }
            ],
            [
                {
                    '-or' => [
                        {
                            'entity_id'   => 'two',
                            'entity_type' => 'fake2'
                        }
                    ],
                    'capability' => [qw(read write manage)],
                    'groupname'  => 'group2',
                    'scope'      => 'type'
                },
                { 'alias' => 'perm_group2_type' }
            ],
            [
                {
                    '-or' => [
                        {
                            'entity_id'   => 'one',
                            'entity_type' => 'fake1'
                        }
                    ],
                    'capability' => [qw(read write manage)],
                    'groupname'  => undef,
                    'scope'      => 'instance'
                },
                { 'alias' => 'perm_undef_instance' }
            ],
            [
                {
                    '-or' => [
                        {
                            'entity_id'   => 'two',
                            'entity_type' => 'fake2'
                        }
                    ],
                    'capability' => [qw(read write manage)],
                    'groupname'  => undef,
                    'scope'      => 'type'
                },
                { 'alias' => 'perm_undef_type' }
            ]
        ],
        'acl_items called ObjectAclEntry->search_rs correctly'
    );
}

package Zaaksysteem::Test::Search::Acl::Mock {
    use Moose;
    with 'Zaaksysteem::Search::Acl';

    has result_source => ( is => 'rw' );
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
