package Zaaksysteem::Test::Search::ESQuery;

use Zaaksysteem::Test;

use Zaaksysteem::Search::ESQuery;

sub test_es_query_parser {
    my $test = shift;

    my $esq = Zaaksysteem::Search::ESQuery->new(
        object_type => 'object'
    );

    my $match_all = $esq->unroll_expression;

    ok !defined $match_all, 'undefined expression';

    $match_all = $esq->unroll_expression({});

    ok !defined $match_all, 'empty expression';

    $match_all = $esq->unroll_expression({ query => { match_all => {} } });

    ok !defined $match_all, 'match_all expression';

    my $term = $esq->unroll_expression({
        query => { term => { a => 1 } }
    });

    is $term->operator, '=', 'term operator';

    isa_ok $term->lterm, 'Zaaksysteem::Search::Term::Column', 'term column set';
    is $term->lterm->value, 'case.a', 'term column match';

    isa_ok $term->rterm, 'Zaaksysteem::Search::Term::Literal', 'term literal set';
    is $term->rterm->value, 1, 'term literal match';

    $term = $esq->unroll_expression({
        query => { term => { a => { query => 1 } } }
    });

    is $term->operator, '=', 'term query operator';

    isa_ok $term->lterm, 'Zaaksysteem::Search::Term::Column', 'term query column set';
    is $term->lterm->value, 'a', 'term column match';

    isa_ok $term->rterm, 'Zaaksysteem::Search::Term::Literal', 'term query literal set';
    is $term->rterm->value, 1, 'term literal match';

    $term = $esq->unroll_expression({
        query => { term => { a => 1, b => 2 } }
    });

    is $term->operator, 'and', 'conjoined term operator';

    isa_ok $term->lterm, 'Zaaksysteem::Search::Conditional', 'conjoined term lterm';
    isa_ok $term->rterm, 'Zaaksysteem::Search::Conditional', 'conjoined term rterm';

    dies_ok { $esq->unroll_expression({
        query => { term => 1 }
    }) } 'invalid term dies';

    $term = $esq->unroll_expression({
        query => { range => { a => { gte => 10, lte => 20 } } }
    });

    isa_ok $term, 'Zaaksysteem::Search::Conditional', 'compound range term';
    is $term->operator, 'and', 'compound range term operator';

    isa_ok $term->lterm, 'Zaaksysteem::Search::Conditional', 'compound range term lterm';
    isa_ok $term->rterm, 'Zaaksysteem::Search::Conditional', 'compound range term rterm';
    isa_ok $term->lterm->lterm, 'Zaaksysteem::Search::Term::Column', 'compound range term lterm lterm column';
    is $term->lterm->lterm->value, 'case.a', 'compound range term lterm lterm column value';
    isa_ok $term->lterm->rterm, 'Zaaksysteem::Search::Term::Literal', 'compound range term lterm rterm literal';
    is $term->lterm->rterm->value, 10, 'compound range term lterm rterm literal value';
    isa_ok $term->rterm->lterm, 'Zaaksysteem::Search::Term::Column', 'compound range term rterm lterm column';
    is $term->rterm->lterm->value, 'case.a', 'compound range term rterm lterm column value';
    isa_ok $term->rterm->rterm, 'Zaaksysteem::Search::Term::Literal', 'compound range term rterm rterm literal';
    is $term->rterm->rterm->value, 20, 'compound range term rterm rterm literal value';

    $term = $esq->unroll_expression({
        query => { bool => {
            must => { term => { a => 1 } },
            must_not => { term => { b => 2 } },
            filter => { term => { c => 3 } }
        }}
    });

    dies_ok { $esq->unroll_expression({
        query => { bool => { should => { term => { a => 1 } } } }
    }) } 'compound bool should query';

    isa_ok $term, 'Zaaksysteem::Search::Conditional', 'compound bool query';
    is $term->operator, 'and', 'compound bool query operator';

    isa_ok $term->lterm, 'Zaaksysteem::Search::Conditional', 'compound bool query lterm';
    isa_ok $term->rterm, 'Zaaksysteem::Search::Conditional', 'compound bool query lterm';
    isa_ok $term->lterm->lterm, 'Zaaksysteem::Search::Conditional', 'compound bool query lterm';
    isa_ok $term->lterm->rterm, 'Zaaksysteem::Search::Conditional', 'compound bool query lterm';
    isa_ok $term->lterm->lterm->lterm, 'Zaaksysteem::Search::Term::Column', 'compound bool query lterm';
    isa_ok $term->lterm->lterm->rterm, 'Zaaksysteem::Search::Term::Literal', 'compound bool query lterm';
    isa_ok $term->lterm->rterm->lterm, 'Zaaksysteem::Search::Term::Column', 'compound bool query lterm';
    isa_ok $term->lterm->rterm->rterm, 'Zaaksysteem::Search::Term::Literal', 'compound bool query lterm';

    $term = $esq->unroll_expression({
        query => { match => { _all => 'abc' } }
    });

    isa_ok $term, 'Zaaksysteem::Search::Conditional::Keyword', 'base match query';
    is $term->value, 'abc', 'base match query keyword';

    dies_ok {
        $esq->unroll_expression({
            query => { match => { column => 'abc' } }
        })
    } 'non-global match unsupported';

    dies_ok {
        $esq->unroll_expression({
            query => { match => 1 }
        })
    } 'match query invalid input';

    $term = $esq->unroll_expression({
        query => {
            bool => {
                must => {
                    term => { a => 1 },
                    match => { _all => 'abc' }
                }
            }
        }
    });

    isa_ok $term, 'Zaaksysteem::Search::Conditional', 'compound term/match query';
    is $term->operator, 'and', 'compound term/match query operator';

    isa_ok $term->lterm, 'Zaaksysteem::Search::Conditional::Keyword', 'compound term/match query lterm';
    isa_ok $term->rterm, 'Zaaksysteem::Search::Conditional', 'compound term/match query rterm';
    is $term->rterm->operator, '=', 'compound term/match query rterm operator';

    $term = $esq->unroll_expression({
        query => {
            bool => {
                must => { term => { a => 1 } },
                must_not => { term => { b => 2 } }
            }
        }
    });

    isa_ok $term, 'Zaaksysteem::Search::Conditional', 'compound must/must_not query';
    is $term->operator, 'and', 'compound must/must_not query operator';

    isa_ok $term->lterm, 'Zaaksysteem::Search::Conditional', 'compound must/must_not query lterm';
    isa_ok $term->rterm, 'Zaaksysteem::Search::Conditional', 'compound must/must_not query rterm';

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
