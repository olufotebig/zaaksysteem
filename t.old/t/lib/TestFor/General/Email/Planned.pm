package TestFor::General::Email::Planned;
use base qw(ZSTest);

use TestSetup;

use Zaaksysteem::Email::Planned;

sub get_pending_emails : Tests {
    $zs->txn_ok(
        sub {
            $zs->create_interface_ok(
                module => 'email',
                name   => 'Ingeplande email',
            );

            my $bn  = $zs->create_notificatie_ok();
            my $mw  = $zs->create_medewerker_ok()->subject_id;
            my $bid = 'betrokkene-medewerker-' . $mw->id;

            my $case = $zs->create_case_ok();


            my $pe = Zaaksysteem::Email::Planned->new(
                case => $case,
            );

            my $result = $pe->create(
                email                   => 'foo@example.com',
                recipient_type          => 'medewerker',
                behandelaar             => $bid,
                bibliotheek_notificatie => $bn,
                scheduled_for           => DateTime->now->add(days => 2),
            );

            my $list = $pe->get_pending_emails;
            is(@$list, 1, "Got one pending e-mail");

        },
        "Get pending e-mails"
    );
}

1;

__END__

=head1 NAME

TestFor::General::Email::Planned - Test planned e-mail retreival

=head1 DESCRIPTION

=head1 SYNOPSIS

    ./zs_prove -v t/lib/TestFor/General/Email/Planned.pm

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
