/*global angular*/
(function ( ) {
	
	angular.module('MintJS.form')
		.directive('mlFormFieldTemplate', [ function ( ) {
			
			return {
				templateUrl: '/html/form/ml-form.html#field',
				replace: true
			};
			
		}]);
	
})();
