/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.object')
		.factory('objectRelationService', [ '$q', '$http', 'objectService', function ( $q, $http, objectService ) {
			
			var objectRelationService = {};
			
			objectRelationService.createRelation = function ( object, relationTypeA, relationTypeB ) {
				var relation;
				
				if(relationTypeA === undefined) {
					relationTypeA = 'related';
				}
				
				if(relationTypeB === undefined) {
					relationTypeB = relationTypeA;
				}
				
				relation = {
					related_object_id: object.id,
					related_object_type: object.type,
					relationship_name_a: relationTypeA,
					relationship_name_b: relationTypeB,
					related_object: object
				};
					
				return relation;
			};
			
			// more explicit name
			objectRelationService.createRelationTo = function ( object, relationTypeA, relationTypeB ) {
				return objectRelationService.createRelation(object, relationTypeA, relationTypeB);	
			};
			
			objectRelationService.relateTo = function ( object, target, relationType ) {
				var relation = objectRelationService.createRelationTo(target, relationType),
					deferred = $q.defer();
				
				object.related_objects.push(relation);
				
				$http({
					method: 'POST',
					url: '/api/object/' + object.id,
					data: {
						related_object_id: target.id,
						name: relation.relationship_name_a
					}
				})
					.success(function ( response ) {
						deferred.resolve(response.result[0]);
					})
					.error(function ( response ) {
						_.pull(object.related_objects, relation);
						deferred.reject(response);
					});
					
				return deferred.promise;
			};
			
			objectRelationService.addRelation = function ( object, relation ) {
				var relations = object.related_objects.concat([relation]),
					current = object.related_objects.concat();
				
				object.related_objects = relations;
				
				return objectService.saveObject(object, { deep_relations: true })
					['catch'](function ( err ) {
						object.related_objects = current;
						throw err;
					});
			};
			
			objectRelationService.removeRelation = function ( object, relation ) {
				var current = object.related_objects.concat();
				
				_.pull(object.related_objects, relation);
				
				return objectService.saveObject(object, { deep_relations: true })
					['catch'](function ( err ) {
						object.related_objects = current;
						throw err;
					});
			};
			
			objectRelationService.isRelated = function ( parent, related ) {
				var relation = _.find(parent.related_objects, { type: related.type, id: related.id }),
					isRelated = !!relation;
				
				return isRelated;
			};
			
			return objectRelationService;
			
		}]);
	
})();
