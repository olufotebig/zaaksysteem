/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.kcc')
		.service('callPollService', [ '$timeout', '$rootScope', 'smartHttp', function ( $timeout, $rootScope, smartHttp ) {
			
			var callPollService = {},
				interval = 5000,
				relevance = 30000,
				timeoutPromise = null;
				
			function poll ( ) {
				smartHttp.connect({
					method: 'GET',
					url: '/api/kcc/call/list',
					params: {
						relevance: relevance/1000
					}
				})
					.success(function ( response ) {
						var calls = response.result;
						$rootScope.$broadcast('call.incoming', calls);
						wait();
					})
					.error(function ( response ) {
						$rootScope.$broadcast('call.poll.error', response.result ? response.result[0] : null);
						wait();
					});
			}
			
			function startPolling ( ) {
				poll();
			}
			
			function stopPolling ( ) {
				if(timeoutPromise) {
					$timeout.cancel(timeoutPromise);
					timeoutPromise = null;
				}
			}
			
			function wait ( ) {
				timeoutPromise = $timeout(poll, interval);
			}
			
			
			callPollService.enable = function ( ) {
				startPolling();	
			};
			
			callPollService.disable = function ( ) {
				stopPolling();
			};
		
			return callPollService;
			
		}]);
	
})();