BEGIN;

CREATE TABLE object_acl_entry (
    uuid UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    object_uuid UUID NOT NULL,
    entity_type TEXT NOT NULL,
    entity_id TEXT NOT NULL,
    permission TEXT NOT NULL
);

ALTER TABLE ONLY object_acl_entry ADD CONSTRAINT object_data_fkey FOREIGN KEY (object_uuid) REFERENCES object_data(uuid);

CREATE INDEX object_acl_entry_permission_idx ON object_acl_entry(permission);
CREATE INDEX object_acl_object_uuid_idx ON object_acl_entry(object_uuid);
CREATE INDEX object_acl_entity_idx ON object_acl_entry(entity_type, entity_id);

COMMIT;
