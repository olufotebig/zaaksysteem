BEGIN;

    ALTER TABLE zaaktype_node ADD COLUMN logging_id INTEGER;
    ALTER TABLE ONLY zaaktype_node ADD CONSTRAINT casetype_node_logging_id_fkey FOREIGN KEY (logging_id) REFERENCES logging(id);

COMMIT;
