BEGIN;

    ALTER TABLE bibliotheek_notificaties ADD COLUMN uuid UUID NOT NULL DEFAULT uuid_generate_v4();

COMMIT;
