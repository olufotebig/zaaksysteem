use utf8;
package Zaaksysteem::Schema::ZaakMeta;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ZaakMeta

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<zaak_meta>

=cut

__PACKAGE__->table("zaak_meta");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'zaak_meta_id_seq'

=head2 zaak_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 verlenging

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 opschorten

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 deel

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 gerelateerd

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 vervolg

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 afhandeling

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 stalled_since

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "zaak_meta_id_seq",
  },
  "zaak_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "verlenging",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "opschorten",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "deel",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "gerelateerd",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "vervolg",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "afhandeling",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "stalled_since",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 zaak_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to("zaak_id", "Zaaksysteem::Schema::Zaak", { id => "zaak_id" });


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-28 09:26:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:8TCykW0n/xMarYJY3oaP8g





# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

