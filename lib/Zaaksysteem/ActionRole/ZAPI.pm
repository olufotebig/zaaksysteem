package Zaaksysteem::ActionRole::ZAPI;

use Moose::Role;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::ActionRole::ZAPI - ZAPI actions automagicks

=cut

use BTTW::Tools;

=head1 DESCRIPTION

This role wraps L<Catalyst::Action/execute> in an exception handler so ZAPI
actions can generically throw exceptions and still produce nice ZAPI-styled
responses.

Interpretation of exceptions is handled by
L<Zaaksysteem::ZAPI::Error/new_from_error>.

If the action bears the C<ZAPI> attribute, this wrapped will also dispatch the
request exection to L<Zaaksysteem::View::ZAPI> which allows actions to simply
set the C<zapi> key in L<Catalyst/stash> in order to produce ZAPI-styled JSON
responses.

    package Zaaksysteem::Controller::Foo;

    # ZAPIController adds this role to all actions in the package.
    BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

    sub my_fancy_action : Chained('...') : Path('fancy') : Args(0) : ZAPI {
        my ($self, $c) = @_;

        ...

        $c->stash->{ zapi } = $my_result;
    }

If the executed action does not bear the C<ZAPI> attribute, control flow is
returned to the action's C<execute> method.

=cut

around execute => sub {
    my ($orig, $self, $controller, $c, @args) = @_;

    try {
        $self->$orig($controller, $c, @args);
    } catch {
        # We allow the passthru of Catalyst exceptions since they're
        # used for control flow (WHHHHYYYYYY THO)
        $_->throw if eval { $_->does('Catalyst::Exception::Basic') };

        $c->log->warn(sprintf('Caught ZAPI exception: %s', $_));
        $c->stash->{ zapi } = Zaaksysteem::ZAPI::Error->new_from_error($_);

        $c->detach($c->view('ZAPI'));
    };

    if ($self->attributes->{ ZAPI }) {
        $c->forward('Zaaksysteem::View::ZAPI');
    }
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
