package Zaaksysteem::DB::ResultSet::ZaaktypeRelatie;
use Moose;

use BTTW::Tools qw(dump_terse);

extends 'DBIx::Class::ResultSet', 'Zaaksysteem::Zaaktypen::BaseResultSet';

use constant    PROFILE => {
    required        => [qw/
    /],
    optional        => [qw/
    /],
};

sub _validate_session {
    my $self            = shift;
    my $profile         = PROFILE;
    my $rv              = {};

    $self->__validate_session(@_, $profile);
}


sub _commit_session {
    my $self                    = shift;
    my $node                    = shift;
    my $element_session_data    = shift;

    foreach my $key (keys %$element_session_data) {
        my $item = $element_session_data->{$key};
        # because integers of the format '' are not allowed, null values are cool.
        if(exists $item->{ou_id} && $item->{ou_id} eq '') {
            delete $item->{ou_id};
        }
        $item->{eigenaar_type} ||= 'aanvrager';

        $item->{copy_subject_role} //= 0;
        # It is a checkbox and.. meh
        if ($item->{copy_subject_role} eq 'on') {
            $item->{copy_subject_role} = 1;
        }

        if ($item->{copy_subject_role} && $item->{subject_role}) {
            if(!ref $item->{subject_role}) {
                $item->{subject_role} = [ $item->{subject_role} ];
            }
        }
        else {
            delete $item->{subject_role};
        }

        # Force undef to be false
        $item->{show_in_pip} //= 0;
        # I believe this is non-functioning behaviour?
        $item->{can_advance_parent} //=0;

        # Delete the label if the bit is not set.
        # We don't want users to come in enable PIP
        # and see whatever what was set ages ago.
        delete $item->{pip_label} if !$item->{show_in_pip};
    }

    $self->next::method(
        $node,
        $element_session_data,
        {
            status_id_column_name   => 'zaaktype_status_id',
        }
    );
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

