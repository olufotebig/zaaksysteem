package Zaaksysteem::Object::Types::Roles::Address;
use Moose::Role;

with qw (
    Zaaksysteem::Object::Roles::Relation
);

=head1 NAME

Zaaksysteem::Object::Types::Roles::Address - A role for subject addresses

=head2 address_correspondence

B<Type>: L<Zaaksysteem::Object::Types::Address>

The related correspondence address for this subject.

=cut

has address_correspondence => (
    is        => 'rw',
    isa       => 'Zaaksysteem::Object::Types::Address',
    traits    => [qw(OR)],
    embed     => 1,
    label     => 'Correspondence address',
    required  => 0,
    clearer   => 'clear_address_correspondence',
    predicate => 'has_address_correspondence',
);

=head2 address_residence

B<Type>: L<Zaaksysteem::Object::Types::Address>

The related residential address for this subject.

=cut

has address_residence => (
    is        => 'rw',
    isa       => 'Zaaksysteem::Object::Types::Address',
    traits    => [qw(OR)],
    embed     => 1,
    label     => 'Residence address',
    required  => 0,
    clearer   => 'clear_address_residence',
    predicate => 'has_address_residence',
);

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
