=head1 NAME

Zaaksysteem::Manual::API::V1::Types::Exception - Type definition for exception
objects

=head1 DESCRIPTION

This page documents the serialization for C<exception> objects.

Objects of this type C<MAY> be returned as the primary
L<result|Zaaksysteem::Manual::API::V1::Response/result> of any
L<request|Zaaksysteem::Manual::API::V1::Request> when an exception state is
triggered.

=head1 JSON

=begin javascript

{
    "reference": null,
    "type": "exception",
    "instance": {
        "type": "some/namespace/somethings_wrong",
        "message": "Something went wrong while processing the request"
    }
}

=end javascript

=head1 INSTANCE ATTRIBUTES

=head2 type

This attribute contains a namespaced exception type. The usual form should be
C<namespace/action/failure_condition>, where C<namespace/action> describes the
origin of the faul, and C<failure_condition> the generic error encountered.

=head2 message

Contains a human-readable error message. This message B<SHOULD> not be passed
directly to any end user of the client system, but is instead intended for
developers of the client application.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
