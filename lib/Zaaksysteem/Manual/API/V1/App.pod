=head1 NAME

Zaaksysteem::Manual::API::V1::APP - APP related API functions

=head1 Description

This API-document describes the usage of our JSON APP API. Via the APP api, it is possible for external
applications to reach the application configuration from within zaaksysteem.

=head1 URL

The base URL for this API is:

    /api/v1/app

Make sure you use the HTTP Method C<GET> for retrieving, and C<POST> for change requests.

=head1 API

This document is based on the V1 API of Zaaksysteem, more information about the default format
of this API can be found in L<Zaaksysteem::Manual::API::V1>. Please make sure you read this
document before continuing.

=head2 ol_settings

   /api/v1/app/NAME

Retrieve the API configuration for an interface with module_type C<app> and C<module> = NAME

B<Example call>

  curl --anyauth -H "Content-Type: application/json" --digest -u "username:password" https://localhost/api/v1/app/bbvapp

B<Request JSON>

An empty request body suffices

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-6ea909-6f6251",
   "development" : false,
   "result" : {
      "instance" : {
         "id" : 93,
         "interface_config" : {
            "casetype_meeting" : 73,
            "casetype_voorstel_item" : 226,
            "color1" : null,
            "color2" : null,
            "logo" : null,
            "title" : "Testapp"
         },
         "module" : "bbvapp",
         "name" : "APP - BBV"
      },
      "reference" : "93477a03-261d-4db8-bbcb-73d2982943cb",
      "type" : "interface"
   },
   "status_code" : 200
}

=end javascript

=head1 Objects

=head2 interface

The interface is the configuration object for an interface

B<Properties>

=over 4

=item id

Type: Num

The table id of the configured interface

=item module

Type: String

Systemname of this module

=item name

Title given by the user

=item interface_config

Custom interface configuration for this app

=back

=head1 Support

The data in this document is supported by the following test. Please make sure you use the API as described
in this test. Any use of this API outside the scope of this test is B<unsupported>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
