=head1 SAML - Gemnet

=head2 Identity Provider Interface

=head3 Create

Create the interface by navigating to interface management interface and
clicking on the '+' to add it. B<The name you use here will be used in the
eventual UI to describe the authenticator. Make sure it's something the
customer expects>.

=head4 Example

=for html <img src="/images/manual/saml/create-idp-gemnet.png" />

=head3 Configure

=head4 Betrouwbaarheidsniveau

Select the I<security level> appropriate for this integration. This setting
depends on whatever deal was cut with the provider. Level 3 selects a
two-factor authentication level, where the user must login with user/password
credentials as well as a transaction code provided by a second path (usually
SMS).

=head4 SSO Binding

This option allows the SSO binding method to be changed. Always set it to
B<HTTP Redirect>.

=head4 SAML Implementatie

Set this to the name of the provider of the IdP we're talking to, Gemnet
renamed itself to B<KPN Lokale Overheid> some time ago, so use that in case of
the intent to establish an eHerkenning integration.

=head4 SAML Metadata URL

Set this to one of the links found L</Metadata>. For the forseeable future this
value will always be B<HM Koppelvlak 1.5>. Gemnet has been known to change this
URL ad-hoc without informing us, so in case of a malfunctioning integration,
check that the configured URL actually has metadata content (if it's XML, it
usually means we're okay).

=head4 Entity ID

This field must be filled with the OIN that was supplied during the registration
phase. It can be found on the intakeform for this customer.

=head4 CA Certificaat van de IdP

This item expects a CA Certificate that was used to sign the exchanges and
metadata provided by the provider. Plain-old ASCII (Base64) armored PEM format
expected.

The subject for the current certificate is
C<C=NL, ST=Zuid-Holland, L=Den Haag, O=Gemnet BV, OU=SSL, CN=hmacc.gemnet.nl>,
issued by C<C=NL, OU=SmartXS, O=KPN, CN=KPN Private Root CA> (serial
C<26:8f:45:78:00:00:00:00:00:5a>).

It is B<not> available for download at the time of writing.

Let's hope this certificate remains constant until it's expiration date on the
B<21st of Febuary, 2016>.

=head4 Endpoints

Tick the boxes where you would like this authentication profile to be available
for the end-user.

=head4 Example

=for html <img src="/images/manual/saml/config-idp-gemnet.png" />

=head2 Metadata

Metadata for every version of Gemnet's SAML implementation.

=over 4

=item HM Koppelvlak 1.1

L<https://hmacc.gemnet.nl/metadata/ehhm11gemnetacc-dv.xml>

=item HM Koppelvlak 1.5

L<https://hmacc.gemnet.nl/metadata/ehhm15gemnetacc-v17.xml>

=item HM Koppelvlak 1.7

L<https://hmacc.gemnet.nl/metadata/ehhm17gemnetacc.xml>

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
