package Zaaksysteem::Controller::API::v1::Subject::Cases;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::Subject::Case - Subject-constrained C<case>
object endpoints

=head1 DESCRIPTION

This controller is a collection of actions for retrieving
L<C<case>|Zaaksysteem::Manual::API::V1::Types::Case> instances constrained on a
specific L<C<subject>|Zaaksysteem::Manual::API::V1::Types::Subject> instance.

=cut

use BTTW::Tools;
use Zaaksysteem::API::v1::ArraySet;
use Zaaksysteem::Object::Query;

sub BUILD {
    my $self = shift;

    $self->add_api_context_permission('extern');
}

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/subject/[SUBJECT_UUID]/cases> routing namespace.

=cut

sub base : Chained('/api/v1/subject/instance_base') : PathPart('cases') : CaptureArgs(0) { }

=head2 owned

Retrieve a resultset of cases for which the subject is the requestor.

See L<Zaaksysteem::Controller::API::v1::Case/list> for more information on
the behavior of this action.

=head3 URL Path

C</api/v1/subject/[SUBJECT_UUID]/cases/owned>

=cut

sub owned : Chained('base') : PathPart('owned') : Args(0) {
    my ($self, $c) = @_;

    my $query = qb('case', {
        cond => qb_and(
            qb_eq('requestor', $c->stash->{ subject }),
            qb_in('case.status', [qw[new open stalled resolved]])
        )
    });

    $c->stash->{ case_base_rs } = $c->model('Object')->query_rs($query);

    # Catalyst::go because we need the full chain for the list action
    $c->go('/api/v1/case/list');
}

=head2 open

Retrieve a resultset of open (not-resolved) cases for which the subject is
the requestor.

See L<Zaaksysteem::Controller::API::v1::Case/list> for more information on
the behavior of this action.

=head3 URL Path

C</api/v1/subject/[SUBJECT_UUID]/cases/open>

=cut

sub open : Chained('base') : PathPart('open') : Args(0) {
    my ($self, $c) = @_;
    
    my $query = qb('case', {
        cond => qb_and(
            qb_eq('requestor', $c->stash->{ subject }),
            qb_in('case.status', [qw[new open stalled]])
        )
    });

    $c->stash->{ case_base_rs } = $c->model('Object')->query_rs($query);

    # Catalyst::go because we need the full chain for the list action
    $c->go('/api/v1/case/list');
}

=head2 related

Retrieve a resultset of cases related to the subject.

See L<Zaaksysteem::Controller::API::v1::Case/list> for more information on
the behavior of this action.

=head3 URL Path

C</api/v1/subject/[SUBJECT_UUID]/cases/related>

=cut

sub related : Chained('base') : PathPart('related') : Args(0) {
    my ($self, $c) = @_;

    # SPLIT-BRAIN: case objects need full subject embedding
    my @case_numbers = $c->model('DB::ZaakBetrokkenen')->search({
        subject_id => $c->stash->{ subject }->id,
        deleted => undef
    })->get_column('zaak_id')->all;
    # BRAIN-SPLAT

    # No related cases exist, simply return the empty set.
    unless (scalar @case_numbers) {
        $c->stash->{ result } = Zaaksysteem::API::v1::ArraySet->new(content => []);
        $c->detach;
    }

    my $query = qb('case', {
        cond => qb_in('case.number', \@case_numbers)
    });

    $c->stash->{ case_base_rs } = $c->model('Object')->query_rs($query);

    # Catalyst::go because we need the full chain for the list action
    $c->go('/api/v1/case/list');
}

=head2 authorized

Retrieve a resultset of cases for which the subject is authorized.

See L<Zaaksysteem::Controller::API::v1::Case/list> for more information on
the behavior of this action.

=head3 URL Path

C</api/v1/subject/[SUBJECT_UUID]/cases/authorized>

=cut

sub authorized : Chained('base') : PathPart('authorized') : Args(0) {
    my ($self, $c) = @_;

    # SPLIT-BRAIN: case objects need full subject embedding
    my @case_numbers = $c->model('DB::ZaakBetrokkenen')->search({
        subject_id => $c->stash->{ subject }->id,
        deleted => undef,
        pip_authorized => 1
    })->get_column('zaak_id')->all;
    # BRAIN-SPLAT

    # No authorized cases exist, simply return the empty set.
    unless (scalar @case_numbers) {
        $c->stash->{ result } = Zaaksysteem::API::v1::ArraySet->new(content => []);
        $c->detach;
    }

    my $query = qb('case', {
        cond => qb_in('case.number', \@case_numbers)
    });

    $c->stash->{ case_base_rs } = $c->model('Object')->query_rs($query);

    # Catalyst::go because we need the full chain for the list action
    $c->go('/api/v1/case/list');
}

=head2 shared_address

Retrieve a resultset of cases which share the address of the subject.

See L<Zaaksysteem::Controller::API::v1::Case/list> for more information on
the behavior of this action.

=head3 URL Path

C</api/v1/subject/[SUBJECT_UUID]/cases/shared_address>

=cut

sub shared_address : Chained('base') : PathPart('shared_address') : Args(0) {
    my ($self, $c) = @_;

    # Attempt to find the main location for our subject, but we're okay with
    # failures to do so.
    my $location = eval {
        my $bag_object = $c->model('Betrokkene')->get(
            {},
            $c->stash->{ subject }->old_subject_identifier
        )->bag_object;

        return $bag_object->{woonplaats}{openbareruimte}{nummeraanduiding}{id}
    };

    unless (defined $location) {
        # No location, nothing to match on, simply return empty resultset.
        $c->stash->{ result } = Zaaksysteem::API::v1::ArraySet->new(content => []);
        $c->detach;
    }

    my $query = qb('case', {
        # This (C|Sh)ould Be Nicer(tm)
        # The hstore contains "nummeraanduiding-12341934" style BAG identifiers,
        # but the BAG model only gives us the numeric parts of those identifiers.
        # So we have to prefix it ourselves...
        cond => qb_eq(
            'case.case_location.nummeraanduiding' => "nummeraanduiding-$location"
        )
    });

    $c->stash->{ case_base_rs } = $c->model('Object')->query_rs($query);

    # Catalyst::go because we need the full chain for the list action
    $c->go('/api/v1/case/list');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
