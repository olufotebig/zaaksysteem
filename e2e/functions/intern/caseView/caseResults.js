export const resultOptions = $$('.result-options li');

export const getResult = () =>
    resultOptions
        .filter(resultOption =>
            resultOption
                .$('input')
                .isSelected()
                .then(selected =>
                    selected
                )
        )
        .first()
        .getText();

export const getResultStatus = () =>
    resultOptions
        .get(0)
        .$('input')
        .getAttribute('disabled')
        .then(disabled =>
            disabled ? 'disabled' : 'enabled'
        );

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
