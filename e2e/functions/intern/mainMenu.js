export const toggle = () => {
    $('.top-bar-menu-button').click();
};

export const toggleNotifications = () => {
    const notificationButton = '[zs-tooltip="Notificaties"]';
    const notifications = '.notifications';

    toggle();
    $(notifications)
        .isDisplayed()
        .then(
            isDisplayed => {
                return isDisplayed ? isDisplayed : Promise.reject();
            },
            () => $(notificationButton).click()
        );
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
