import angular from 'angular';

export default
	angular.module('officeService', [
	])
		.factory('officeService', [ '$window', ( $window ) => {

			return $window.Office;

		}])
		.name;
