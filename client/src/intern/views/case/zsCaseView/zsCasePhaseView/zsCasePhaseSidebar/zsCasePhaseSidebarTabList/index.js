import angular from 'angular';
import template from './template.html';
import './styles.scss';

export default angular
	.module('zsCasePhaseSidebarTabList', [])
	.component('zsCasePhaseSidebarTabList', {
		bindings: {
			tabs: '&'
		},
		template
	})
	.name;
