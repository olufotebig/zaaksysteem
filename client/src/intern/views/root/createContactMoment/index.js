import angular from 'angular';
import vormFormModule from './../../../../shared/vorm/vormForm';
import radioModule from './../../../../shared/vorm/types/radio';
import textareaModule from './../../../../shared/vorm/types/textarea';
import selectModule from './../../../../shared/vorm/types/select';
import vormObjectSuggestModule from './../../../../shared/object/vormObjectSuggest';
import snackbarServiceModule from './../../../../shared/ui/zsSnackbar/snackbarService';
import seamlessImmutable from 'seamless-immutable';
import capitalize from 'lodash/capitalize';
import startsWith from 'lodash/startsWith';
import template from './template.html';
import './styles.scss';

export default
	angular.module('createContactMoment', [
		vormFormModule,
		radioModule,
		textareaModule,
		selectModule,
		vormObjectSuggestModule,
		snackbarServiceModule
	])
		.directive('createContactMoment', [ '$q', '$http', '$state', 'snackbarService', ( $q, $http, $state, snackbarService ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					onSubmit: '&',
					subject: '&'
				},
				bindToController: true,
				controller: [ function ( ) {

					let ctrl = this,
						fields,
						actions,
						defaults = seamlessImmutable({ medium: 'behandelaar', subject_type: 'natuurlijk_persoon' });

					if (startsWith($state.current.name, 'case')) {

						let caseObj = $state.$current.locals.globals.case.data(),
							subject;

						subject =
							caseObj.instance.requestor && caseObj.instance.requestor.instance.subject_type !== 'medewerker' ?
								{
									label: caseObj.instance.requestor.instance.name,
									data: {
										id: caseObj.instance.requestor.instance.id.split('-')[2]
									},
									type: caseObj.instance.requestor.instance.id.split('-')[1]
								}
								: null;

						defaults = defaults.merge({
							case: {
								label: `${caseObj.instance.number}: ${caseObj.instance.casetype.instance.name}`,
								data: {
									id: caseObj.instance.number
								}
							}
						});

						if (subject) {
							defaults = defaults.merge({
								subject,
								subject_type: subject ? subject.type : null
							});
						}
					}

					if (ctrl.subject() && ctrl.subject().type !== 'medewerker') {

						defaults = defaults.merge({
							subject: ctrl.subject(),
							subject_type: ctrl.subject().type
						});
						
					}

					fields =
						[
							{
								name: 'medium',
								label: 'Contactkanaal',
								template: 'select',
								data: {
									options: 'behandelaar,balie,telefoon,post,email,webformulier,sociale media'.split(',')
										.map(value => {
											return {
												value,
												label: capitalize(value)
											};
										})
								},
								required: true
							},
							{
								name: 'subject_type',
								label: 'Contacttype',
								template: 'radio',
								data: {
									options: [
										{
											value: 'natuurlijk_persoon',
											label: 'Burger'
										},
										{
											value: 'bedrijf',
											label: 'Organisatie'
										}
									]
								},
								required: true
							},
							{
								name: 'subject',
								label: [ '$values', ( values ) => {
									return values.subject_type === 'natuurlijk_persoon' ?
										'Burger'
										: (values.subject_type === 'medewerker' ? 'Medewerker' : 'Organisatie');
								}],
								template: 'object-suggest',
								required: true,
								data: {
									objectType: [ '$values', values => values.subject_type ]
								}
							},
							{
								name: 'case',
								label: 'Zaak',
								template: 'object-suggest',
								data: {
									objectType: 'case'
								}
							},
							{
								name: 'message',
								label: 'Samenvatting',
								template: 'textarea',
								required: true
							}
						];

					actions = [
						{
							type: 'submit',
							label: 'Voeg toe',
							click: [ '$values', ( values ) => {

								let promise =
									snackbarService.wait('Het contactmoment wordt aangemaakt.', {
										promise: $http({
											method: 'POST',
											url: '/api/v1/conversation/create',
											data: {
												type: 'note',
												medium: values.medium,
												message: values.message,
												case_id: values.case ? values.case.data.id : null,
												subject_id: `betrokkene-${values.subject_type}-${values.subject.data.id}`
											}
										}),
										then: ( ) => {
											return {
												message: `Contactmoment aangemaakt ${values.case ? `voor zaak ${values.case.data.id} ` : ''}met ${values.subject.label}`
											};
										},
										catch: ( ) => 'Contactmoment kon niet worden aangemaakt. Neem contact op met uw beheerder voor meer informatie.'
									});

								ctrl.onSubmit( { $promise: promise });

							}]
						}
					];

					ctrl.getFields = ( ) => fields;

					ctrl.getDefaults = ( ) => defaults;

					ctrl.getActions = ( ) => actions;

					ctrl.processChange = ( name, value, values ) => {

						let vals = values;

						if (name === 'subject_type') {
							vals = values.merge({ subject: null });
						}

						return vals;

					};

					ctrl.getPrefillName = ( ) => {

						return defaults.subject ?
							defaults.subject.label
							: '';
					};

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
