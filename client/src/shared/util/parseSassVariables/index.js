const regex = '\\$([A-Za-z0-9\-]+):\s*(.*?);';
const multi = new RegExp(regex, 'gm');
const single = new RegExp(regex);

export default ( source = '' ) => {
	const vars = {};
	let line;

	while ((line = multi.exec(source))) {
		const match = single.exec(line);
		const key = match[1];
		const value = match[2]
			.replace(/\s*!default$/, '')
			.replace(/\"(.*)\"/, '$1')
			.replace(/#{\$([A-Za-z0-9\-]+)}/g, ( m, ref, offset, string ) => {
				if (!(ref in vars)) {
					throw new Error(`Unable to parse string: reference ${ref} not found in ${string}`);
				}

				return vars[ref];
			})
			.trim();

		vars[key] = value;
	}

	return vars;
};
