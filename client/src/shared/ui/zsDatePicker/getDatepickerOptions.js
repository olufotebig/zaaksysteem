import i18n from './i18nConstants';
import getYearMonthNumber from '../../util/date/getYearMonthNumber';
import fromIsoCalendarDate from '../../util/date/fromIsoCalendarDate';

/**
 * Get Pikaday options from the HTMLInputElement.
 * Pikaday is used as polyfill when input type date is not supported.
 * @see https://github.com/dbushell/Pikaday
 *
 * @param {HTMLInputElement} field
 * @return {Object}
 * @property {HTMLInputElement} field
 * @property {Date} minDate
 * @property {Date} maxDate
 * @property {Date} defaultDate
 * @property {number} firstDay
 * @property {string} format
 * @property {Object} i18n
 */
function getDatepickerOptions( field ) {
	const { min, max } = field;
	const maxDate = fromIsoCalendarDate(max);
	let minDate, defaultDate;

	if (min) {
		const now = getYearMonthNumber(new Date());

		minDate = fromIsoCalendarDate(min);

		// Pikaday defaults to the current month even if it is not in the min/maxDate range,
		// so explicitly set defaultDate to the closest date if necessary.
		if (getYearMonthNumber(minDate) > now) {
			defaultDate = minDate;
		} else if (getYearMonthNumber(maxDate) < now) {
			defaultDate = maxDate;
		}
	}

	return {
		field,
		minDate,
		maxDate,
		defaultDate,
		firstDay: 1,
		format: 'DD-MM-YYYY',
		i18n
	};
}

export default getDatepickerOptions;
