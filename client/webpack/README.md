# webpack

## Build vendors bundle

The vendors bundle is under source control[1].
You only need to build it again when you add, 
remove or update a vendor dependency.

It is copied to `/root/assets` by either the 
`prebuild-apps` or `prestart` npm script. 

    $ npm run build-vendors

## Build app bundles

### Production build

- compress JS 
- mangle JS indentifiers
- minimize CSS


    $ npm run build-apps
    
### Production build for debugging

- without service workers
- without JS identifier mangling
- with JS source map
- with CSS source map


    $ npm run build-apps -- --env debug

## Development

    $ npm start

## Notes

[1]: This is a flaw in the webpack DLL plugin.
     You'd rather want to publish and install 
     the vendor bundle and its manifest, 
     but reading the manifest requires the 
     vendor dependencies to be installed 
     in the project.
