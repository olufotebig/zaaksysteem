/**
 * Simplify retrieval of npm package config variables.
 */
const { env } = process;
const configExpression = /^npm_package_config_(.*)/;
const magicValueExpression = /^(true|false|null)$/;

function getJsonValue( input ) {
  if (magicValueExpression.test(input)) {
    return JSON.parse(input);
  }

  return input;
}

const reducer = ( bucket, key ) => {
  const match = configExpression.exec(key);

  if (match) {
    const packageKey = match[1];

    bucket[packageKey] = getJsonValue(env[key]);
  }

  return bucket;
};

const packageConfig = Object
  .keys(env)
  .reduce(reducer, {});

module.exports = packageConfig;
